const express = require("express");
const app = express();

//importing app routes files
const orgRoutes = require("./routes/organisations-routes");
const authRoutes = require("./routes/auth-routes");
const employeeRoutes = require("./routes/employee-routes");
const adminRoutes = require("./routes/admin");

//importing environment file
require("custom-env").env(process.env.NODE_ENV, "./env");
const port = process.env.APP_PORT || 8042;
const bodyParser = require("body-parser");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const mongoose = require("mongoose");
const mongoUri = process.env.MONGOURL;

// connecting mongodb
mongoose.connect(
  mongoUri,
  { useNewUrlParser: true, useUnifiedTopology: true },
  function (error) {
    if (error) {
      console.log("error occured on mongo connection - ", error);
      process.exit();
    }
    console.log("database connected!");
  }
);

// mounting routes

app.use("/api/v1/authentication", authRoutes);
app.use("/api/v1/organization", orgRoutes);
app.use("/api/v1/employees", employeeRoutes);
app.use("/api/v1/admin", adminRoutes);

// launching app
app.listen(port);
console.log("Server is listening at " + port);

exports = module.exports = app;
