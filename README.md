This is a Multi-Tenant Attendance Software built for Internal Use at Webmavericks Softcoders Private Limited!

This application is built on NodeJS and Mongo Database !

Software is built with keeping in mind that many organizations can signup on the portal and then add their employees. Once employees are added , employees can login on the software and log their check-in , check-out time as well as they can keep the track of their days off and their leaves applications !

This software also supports employee management system ! Organisation Admin can add , update , view and delete their employees' profiles , attendance and leaves applications ! 