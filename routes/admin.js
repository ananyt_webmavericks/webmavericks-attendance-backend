const express = require("express");
const router = express.Router();
const adminController = require("../controllers/admin");
const checkJWT = require("../middlewares/check-jwt");
const adminMiddleware = require("../middlewares/admin");

router.post(
  "/create-admin",
  adminMiddleware.createAdmin,
  adminController.createAdmin
);
router.post("/login", adminMiddleware.validateLogin, adminController.login);
router.post(
  "/change-password",
  checkJWT.validateAdmin,
  adminMiddleware.validateChangePassword,
  adminController.changePassword
);

router.patch(
  "/approve-organization/:id",
  checkJWT.validateAdmin,
  adminMiddleware.validateApproveOrganization,
  adminController.approveOrganization
);
router.get(
  "/get-all-organizations",
  checkJWT.validateAdmin,
  adminController.getAllOrganizations
);
router.patch(
  "/set-organization-status",
  checkJWT.validateAdmin,
  adminMiddleware.validateChangeOrganizationStatus,
  adminController.changeOrganizationStatus
);

module.exports = router;
