const nodemailer = require("nodemailer");

const sendMail = async (toAddress, password) => {
  try {
    const mailOptions = {
      from: process.env.MAIL_SENDER,
      to: toAddress,
      subject: "Your One Time Password for Employee Management System",
      replyTo: toAddress,
      html: `Your Password is ${password}`,
    };

    const transporter = nodemailer.createTransport({
      service:process.env.MAIL_SERVICE,
      auth:{
        user:process.env.MAIL_SENDER,
        pass:process.env.MAIL_PASSWORD
      },
      requireTLS:false
    });

    transporter.sendMail(mailOptions, function (error, response) {
      if (error) {
        console.log(error);
      }
    });
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  sendMail,
};
