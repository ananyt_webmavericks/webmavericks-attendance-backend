const mongoose = require("mongoose");
const constants = require("../config/constants.json");
const admin = require("./admin");

const isValidName = async (name) => {};

const isValidNumber = async (number) => {};

const isValidSuperAdmin = async (superAdminId) => {};

const organizationSchema = mongoose.Schema(
  {
    orgName: { type: String, required: true, validate: isValidName },
    orgContactEmail: { type: String, required: true },
    orgContactPerson: { type: String, required: true, validate: isValidName },
    orgContactNumber: {
      type: String,
      required: true,
      length: 10,
      validate: isValidNumber,
    },
    orgAddress: {
      building: { type: String, required: true },
      street: { type: String, required: true },
      landmark: { type: String },
      city: { type: String, required: true },
      pincode: { type: String, required: true, validate: isValidNumber },
      state: { type: String, required: true },
      country: { type: String, required: true },
    },
    verified: {
      type: Boolean,
      default: constants.STATUS.INACTIVE,
      required: true,
      enum: [constants.STATUS.INACTIVE, constants.STATUS.ACTIVE],
    },
    passwordChanged: {
      type: Boolean,
      default: constants.STATUS.INACTIVE,
      enum: [constants.STATUS.INACTIVE, constants.STATUS.ACTIVE],
    },
    approved: {
      type: Boolean,
      default: constants.STATUS.INACTIVE,
      required: true,
      enum: [constants.STATUS.INACTIVE, constants.STATUS.ACTIVE],
    },
    approvedBy: {
      type: mongoose.Schema.ObjectId,
      ref: "admin",
      validate: isValidSuperAdmin,
    },
    approvedOn: { type: String },
    status: {
      type: Boolean,
      default: constants.STATUS.INACTIVE,
      enum: [constants.STATUS.INACTIVE, constants.STATUS.ACTIVE],
    },
  },
  { timestamps: true }
);

organizationSchema.index(
  { orgName: 1 },
  { unique: true, name: "IDX_ORG_NAME" }
);
organizationSchema.index(
  { orgContactNumber: 1 },
  { unique: true, name: "IDX_ORG_CONTACT" }
);
organizationSchema.index(
  { orgContactEmail: 1 },
  { unique: true, name: "IDX_ORG_EMAIL" }
);

module.exports = mongoose.model("organizations", organizationSchema);
