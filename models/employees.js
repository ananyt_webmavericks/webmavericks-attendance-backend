const mongoose = require("mongoose");
const constants = require("../config/constants.json");
const organization = require("./organizations");

const isValidName = async (name) => {};

const isValidEmail = async (email) => {};

const isValidOrganization = async (organizationId) => {};

const employeeSchema = mongoose.Schema(
  {
    name: { type: String, required: true, validate: isValidName },
    email: { type: String, required: true, validate: isValidEmail },
    status: {
      type: String,
      default: constants.STATUS.INACTIVE,
      required: true,
      enum: [constants.STATUS.INACTIVE, constants.STATUS.ACTIVE],
    },
    organizationId: {
      type: mongoose.Schema.ObjectId,
      ref: "organizations",
      required: true,
      validate: isValidOrganization,
    },
    password: { type: String, required: true, length: 8 },
    passwordChanged: {
      types: Boolean,
      required: true,
      default:constants.STATUS.INACTIVE,
      enum: [constants.STATUS.INACTIVE, constants.STATUS.ACTIVE],
    },
    profile: {
      dateOfJoining: { type: String },
      dateOfBirth: { type: String },
      designation: { type: String },
      mobileNumber: { type: String, length: 10 },
      alternateMobileNumber: { type: String, length: 10 },
      address: {
        building: { type: String },
        street: { type: String },
        landmark: { type: String },
        city: { type: String },
        pincode: { type: String },
        state: { type: String },
        country: { type: String },
      },
    },
  },
  { timestamps: true }
);

organizationSchema.index(
  { email: 1 },
  { unique: true, name: "IDX_EMPLOYEE_EMAIL" }
);

module.exports = mongoose.model("employees", employeeSchema);
